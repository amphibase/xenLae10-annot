#!/bin/bash
UP_VER=$(head -n 1 raw/relnotes.txt | awk '{print $3}' )
OUT_FILE="XENLA_xenLae10.UniProt_"$UP_VER".prot_all.fa"

echo "Make "$OUT_FILE

./02a.uniprot-cleanup-proteome.py raw/UP000186698_8355.fasta.gz > $OUT_FILE
./02a.uniprot-cleanup-proteome.py raw/UP000186698_8355_additional.fasta.gz >> $OUT_FILE
./02b.make-non_redundant_fasta.py $OUT_FILE
