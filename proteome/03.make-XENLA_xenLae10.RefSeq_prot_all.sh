#!/bin/bash
REFSEQ_VER=$(ls raw/README_*release* | awk -F"release_" '{print $2}')
OUT_FILE="XENLA_xenLae10.RefSeq_"$REFSEQ_VER".prot_all.fa"

echo "Make "$OUT_FILE

./03a.make-XENLA_xenLae10.RefSeq_prot_all.py > $OUT_FILE
./02b.make-non_redundant_fasta.py $OUT_FILE
