#!/usr/bin/env python3
import sys

filename_log = sys.argv[1]

f_log = open(filename_log, 'r')
for line in f_log:
    tokens = line.strip().split("\t")
    if len(tokens) != 3:
        continue

    for tmp in tokens[2].split(';;'):
        (tmp_name, tmp_acc) = tmp.split('|')
        print("%s\t%s\tIdentical sequence: %s" % (tmp_acc, tmp_name, tmp_acc))
f_log.close()
